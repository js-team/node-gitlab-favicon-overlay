import { FaviconOverlayManager } from "../src";

let isOn = false;

const handleMouseMove = (event: MouseEvent) => {
  const overlay = FaviconOverlayManager.getOverlay(0);

  const { clientX, clientY } = event;
  overlay.position = {
    centered: true,
    x: (32 * clientX) / window.innerWidth,
    y: (32 * clientY) / window.innerHeight,
  };
};

const turnOn = () => {
  if (isOn) {
    return;
  }

  const overlayImage = document.getElementById("overlay") as HTMLImageElement;
  FaviconOverlayManager.setFaviconOverlay(overlayImage.src);
  document.addEventListener("mousemove", handleMouseMove);

  isOn = true;
};

const turnOff = () => {
  if (!isOn) {
    return;
  }

  document.removeEventListener("mousemove", handleMouseMove);
  FaviconOverlayManager.resetFaviconOverlay();

  isOn = false;
};

const toggle = () => {
  if (isOn) {
    turnOff();
  } else {
    turnOn();
  }
};

const main = () => {
  FaviconOverlayManager.initialize().then(() => {
    document.addEventListener("keydown", (event: KeyboardEvent) => {
      if (event.key === " ") {
        toggle();
      }
    });

    turnOn();
  });
};

main();
